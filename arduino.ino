#include "LedControl.h"

// digital pins for LED display
#define DIN 7 //blue
#define CLK 6 //yellow
#define LOAD 5 //green
// analog pin used to read photoresistor value
#define AIN A5

// what should the photoresistor read when laser is on?
#define AMBIENT_LIGHT 100
#define AMBIENT_LASER 300

// number of devices is 1
LedControl lc=LedControl(DIN,CLK,LOAD,1);

int inchesPerSecond = 0;
int count = 0;
int digit = 10;
int objLength = 3; // object length in inches
bool setDecimal = false;

void setup() {
  // set photoresistor input pin
  pinMode(AIN, INPUT);
  Serial.begin(9600);

  // setup 7 segment display
  lc.shutdown(0,false);   // Enable display
  lc.setIntensity(0,15);  // Set brightness level (0 is min, 15 is max)
  lc.clearDisplay(0);     // Clear display register
}

void loop() {
  // read the photoresistor value and write it to serial (debug)
  int prVal = analogRead(AIN);
  //Serial.println(prVal);

  if(prVal < AMBIENT_LIGHT){
    // the light it is obstructed
    count = count+1;
    delay(1);
  }
  else{
    // how many seconds it was shaded
    if(count > 0){
      float t = (float)count/1000;
      if(t>objLength){
        // the object is moving slower than objLength inches per seconds
        float ips = objLength/t;
        //Serial.println(ips);
        inchesPerSecond = ips*100;
        //Serial.println(inchesPerSecond);
        setDecimal = true;
      }
      else{
        inchesPerSecond = objLength/t;
        //Serial.println(t);
        //Serial.println(inchesPerSecond);
      }

      // get all the digits for the display
      int ones=inchesPerSecond%10;
      inchesPerSecond=inchesPerSecond/10;
      int tens=inchesPerSecond%10;
      inchesPerSecond=inchesPerSecond/10;
      int hundreds=inchesPerSecond;
      lc.setDigit(0,2,hundreds,setDecimal);
      lc.setDigit(0,1,tens,false);
      lc.setDigit(0,0,ones,false);
      delay(3000);
      lc.clearDisplay(0);
      // reset count
      count = 0;
      setDecimal = false;
    }
  }

}